if ENV['DRONE']
  puts "\t*** drone logging initialized ***"
  $stdout.sync = true
  @logger = Logger.new $stdout
  @logger.level = Logger::DEBUG
  ActiveRecord::Base.logger = @logger
  Rails.logger = @logger
end
