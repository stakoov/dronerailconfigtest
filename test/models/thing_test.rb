require 'test_helper'

class ThingTest < ActiveSupport::TestCase
  test "create a thing" do
    Thing.create(name: 'foo', slug: 'bar')
    assert_equal 1, Thing.count
  end

end
