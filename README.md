## drone-rails-pg

A working sample app that runs on [drone.io] (open source edition) using the postgres service.

[drone.io]: https://github.com/drone/drone
